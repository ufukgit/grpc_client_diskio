#include "diskstatschart.h"
#include "ui_diskstatschart.h"

DiskStatsChart::DiskStatsChart(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::DiskStatsChart)
{
	ui->setupUi(this);
	createChart();
}

void DiskStatsChart::createChart()
{
	chart = new QChart();
	seriesRead = new QLineSeries();
	seriesRead->setName(" Read Speed (Mb/s) ");
	chart->addSeries(seriesRead);
	seriesWrite = new QLineSeries();
	seriesWrite->setName(" Write Speed (Mb/s) ");
	chart->addSeries(seriesWrite);
	seriesReadPower = new QLineSeries();
	seriesReadPower->setName(" ReadPowerOn (Mb) ");
	chart->addSeries(seriesReadPower);
	seriesTx = new QLineSeries();
	seriesTx->setName(" Tx (Kbps) ");
	chart->addSeries(seriesTx);
	seriesRx = new QLineSeries();
	seriesRx->setName(" Rx (Kbps) ");
	chart->addSeries(seriesRx);
	chart->setTitle(" DISK IO ");
	chart->legend()->setVisible(true);
	chart->legend()->setAlignment(Qt::AlignBottom);
	chartView = new QChartView(chart);
	chartView->setRenderHint(QPainter::Antialiasing);
	chartView->setParent(ui->horizontalFrame);
	ui->tabWidget->setTabText(0, "Disks");
	ui->tabWidget->setTabText(1, "Interfaces");
	axisX = new QDateTimeAxis;
	axisX->setTickCount(7);
	axisX->setFormat("hh:mm:ss");
	chart->addAxis(axisX, Qt::AlignBottom);
	seriesRead->attachAxis(axisX);
	seriesWrite->attachAxis(axisX);
	seriesReadPower->attachAxis(axisX);
	seriesTx->attachAxis(axisX);
	seriesRx->attachAxis(axisX);
	axisY = new QValueAxis;
	chart->addAxis(axisY, Qt::AlignLeft);
	seriesRead->attachAxis(axisY);
	seriesReadPower->setColor(Qt::black);
	seriesWrite->attachAxis(axisY);
	seriesReadPower->attachAxis(axisY);
	seriesTx->attachAxis(axisY);
	seriesRx->attachAxis(axisY);
	seriesRx->setColor(Qt::white);
	axisY->setRange(0, 50);
	chart->axisX()->setMin(QDateTime::currentDateTime().addSecs(-60*1));
	chart->axisX()->setMax(QDateTime::currentDateTime().addSecs(60*1));
	chartView->setRubberBand(QChartView::HorizontalRubberBand);
	chartView->setRubberBand(QChartView::VerticalRubberBand);
	ui->horizontalFrame->setGeometry(QRect(320, 20, 1301, 731));
	chartView->resize(ui->horizontalFrame->size());
	QLinearGradient backgroundGradient;
	backgroundGradient.setStart(QPointF(0, 0));
	backgroundGradient.setFinalStop(QPointF(0, 1));
	backgroundGradient.setColorAt(0.0, QRgb(0xd2d0d1));
	backgroundGradient.setColorAt(1.0, QRgb(0x4c4547));
	backgroundGradient.setCoordinateMode(QGradient::ObjectBoundingMode);
	chart->setBackgroundBrush(backgroundGradient);
	QLinearGradient plotAreaGradient;
	plotAreaGradient.setStart(QPointF(0, 1));
	plotAreaGradient.setFinalStop(QPointF(1, 0));
	plotAreaGradient.setColorAt(0.0, QRgb(0x555555));
	plotAreaGradient.setColorAt(1.0, QRgb(0x55aa55));
	plotAreaGradient.setCoordinateMode(QGradient::ObjectBoundingMode);
	chart->setPlotAreaBackgroundBrush(plotAreaGradient);
	chart->setPlotAreaBackgroundVisible(true);
	ui->interfacesList->setStyleSheet("""QListWidget{ background: grey; }""");
	ui->disksList->setStyleSheet("""QListWidget{ background: grey; }""");
	ui->connectButton->setStyleSheet("""QPushButton{ background: grey; }""");
	ui->tabWidget->setStyleSheet("""QTabWidget{ background: grey; }""");
}

void DiskStatsChart::on_connectButton_clicked()
{
	ui->disksList->clear();
	ui->interfacesList->clear();
	seriesRead->clear();
	seriesWrite->clear();
	seriesReadPower->clear();
	seriesTx->clear();
	seriesRx->clear();
	std::string address;
	address = ui->lineEdit->text().toStdString();
	diskio = new DiskioMaster(address);
	if(!diskio)
		return;
	if (diskio->disks.disk_size() == 0) {
			QMessageBox::information(this, tr("ERROR"), tr("Server is not running"));
			return;
		}
	for (int i = 0; i < diskio->disks.disk_size(); ++i) {
		QString diskName = QString::fromStdString(diskio->disks.disk(i).name());
		ui->disksList->addItem(diskName);
	}
	net = new NetTop(address);
	if(!net)
		return;
	for (int i = 0; i < net->interfaces.interfaces_size(); ++i) {
		QString interfacesName = QString::fromStdString(net->interfaces.interfaces(i));
		ui->interfacesList->addItem(interfacesName);
	}
}

void DiskStatsChart::on_disksList_itemClicked(QListWidgetItem *item)
{
	axisY->setRange(0, 50);
	seriesTx->clear();
	seriesRx->clear();
	countDiskClick++;
	file = item->text().toStdString();
	seriesRead->clear();
	seriesWrite->clear();
	seriesReadPower->clear();
	timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), this, SLOT(showChart()));
	timer->start(1000);
	if(countDiskClick >= 2) {
		if(timer->isActive())
			timer->stop();
	}
	files.clear();
	return;
}

void DiskStatsChart::showChart()
{
	if(!files.empty()){
		file.clear();
		return;
	}
	double readSpeed = diskio->GetDiskStats(file).read();
	double writeSpeed = diskio->GetDiskStats(file).write();
	double readPower = diskio->GetDiskStats(file).readpoweron();
	qint64 currentDate = QDateTime::currentMSecsSinceEpoch();
	double doubReadSpeed = readSpeed/1024;
	seriesRead->append(currentDate, doubReadSpeed);
	double doubWriteSpeed = writeSpeed/1024;
	seriesWrite->append(currentDate, doubWriteSpeed);
	double doubReadPower = readPower/1024;
	seriesReadPower->append(currentDate, doubReadPower);
	axisX->setMin(QDateTime::currentDateTime().addSecs(-30));
	axisX->setMax(QDateTime::currentDateTime().addSecs(30));
	chart->update();
	chartView->update();
	seriesTx->clear();
	seriesRx->clear();
}

void DiskStatsChart::on_interfacesList_itemClicked(QListWidgetItem *items)
{
	axisY->setRange(0, 5000);
	seriesRead->clear();
	seriesWrite->clear();
	seriesReadPower->clear();
	countInterfaceClick++;
	files = items->text().toStdString();
	seriesTx->clear();
	seriesRx->clear();
	timerNet = new QTimer(this);
	connect(timerNet, SIGNAL(timeout()), this, SLOT(showChartNet()));
	timerNet->start(1000);
	if(countInterfaceClick==2 || countInterfaceClick >2) {
		if(timerNet->isActive())
			timerNet->stop();
	}
	file.clear();
	return;
}

void DiskStatsChart::showChartNet()
{
	if(!file.empty()) {
		files.clear();
		return;
	}
	double tX = net->GetBandWidth(files).tx();
	double rX = net->GetBandWidth(files).rx();
	qint64 currentDate = QDateTime::currentMSecsSinceEpoch();
	seriesTx->append(currentDate, tX);
	seriesRx->append(currentDate, rX);
	axisX->setMin(QDateTime::currentDateTime().addSecs(-30));
	axisX->setMax(QDateTime::currentDateTime().addSecs(30));
	chart->update();
	chartView->update();
	seriesRead->clear();
	seriesWrite->clear();
	seriesReadPower->clear();
}

void DiskStatsChart::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::MiddleButton)
	{
		chart->zoomReset();
		chart->mapToValue(event->pos());
		event->accept();
	}
	QMainWindow::mousePressEvent(event);
}

DiskStatsChart::~DiskStatsChart()
{
	delete ui;
	delete timer;
	delete timerNet;
	delete chart;
	delete chartView;
	delete seriesRead;
	delete seriesReadPower;
	delete seriesWrite;
	delete seriesRx;
	delete seriesTx;
	delete axisX;
	delete axisY;
}

