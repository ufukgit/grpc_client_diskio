#include "diskiomaster.h"

DiskioMaster::DiskioMaster(const std::string &endp)
{
	endpoint = endp;
	CreateChannel(endpoint);
	GetDisks();
}

void DiskioMaster::CreateChannel(const std::string &endp)
{
	using grpc::Channel;
	std::shared_ptr<Channel> channel;
	channel = grpc::CreateChannel(endp, grpc::InsecureChannelCredentials());
	stub = DiskioMasterService::NewStub(channel);
}

measuresystems::Disks DiskioMaster::GetDisks()
{
	google::protobuf::Empty empty;
	grpc::ClientContext context;
	grpc::Status statusDisks;
	measuresystems::Disks diskCtrl;
	if (!stub)
		return diskCtrl;
	std::mutex tempDisks;
	std::lock_guard<std::mutex> lockDisks(tempDisks);
	statusDisks = stub->GetDisks(&context, empty, &disks);
	return disks;
}

measuresystems::DiskStats DiskioMaster::GetDiskStats(std::string diskname)
{
	grpc::Status statusStats;
	measuresystems::DiskStats diskStatsResp;
	measuresystems::DiskStats diskStatsCtrl;
	measuresystems::Disk disk;
	grpc::ClientContext contextDiskStats;
	*disk.mutable_name() = diskname;
	statusStats = stub->GetDiskStats(&contextDiskStats, disk, &diskStatsResp);
	if (!stub)
		return diskStatsCtrl;
	return diskStatsResp;
}

DiskioMaster::~DiskioMaster()
{
}
