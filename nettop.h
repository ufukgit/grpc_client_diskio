#ifndef NETTOP_H
#define NETTOP_H

#include <grpc++/grpc++.h>
#include "diskio.grpc.pb.h"
#include <google/protobuf/empty.pb.h>
#include <QObject>

using measuresystems::NetTopService;

class NetTop : public QObject
{
	Q_OBJECT
public:
	NetTop(const std::string &ep);
	~NetTop();
	measuresystems::NetworkInterfaces interfaces;
	measuresystems::BandwidthResponse GetBandWidth(std::string getinterface);
private:
	measuresystems::NetworkInterfaces GetInterfaces();
	void CreateChannel(const std::string &ep);
	std::unique_ptr<NetTopService::Stub> stub = nullptr;
	std::string endpoint;
};

#endif // NETTOP_H
