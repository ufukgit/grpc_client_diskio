QT       += core gui
QT       += network
QT       += core gui charts
QT       += concurrent
QT       += widgets

   QMAKE_CXXFLAGS += -Wl,--stack,100000000
   QMAKE_CXXFLAGS += -Wl,--heap,100000000
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += /usr/local/include  #LIBS te yazılacak dosyaları bu dosya yolunda arar..
LIBS += -L/usr/local/lib
LIBS += -lgrpc++
LIBS += /usr/local/lib/libprotobuf.a

SOURCES += \
    diskiomaster.cpp \
    diskstatschart.cpp \
    main.cpp \
    diskio.pb.cc \
    diskio.grpc.pb.cc \
    nettop.cpp \

HEADERS += \
    diskio.pb.h \
    diskio.grpc.pb.h \
    diskiomaster.h \
    diskstatschart.h \
    nettop.h \

FORMS += \
    diskstatschart.ui \

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    diskio.proto

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../../usr/local/lib/release/ -labsl_synchronization
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../../usr/local/lib/debug/ -labsl_synchronization
else:unix: LIBS += -L$$PWD/../../../../../../usr/local/lib/ -labsl_synchronization

INCLUDEPATH += $$PWD/../../../../../../usr/local/include
DEPENDPATH += $$PWD/../../../../../../usr/local/include
