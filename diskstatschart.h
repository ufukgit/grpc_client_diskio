#ifndef DISKSTATSCHART_H
#define DISKSTATSCHART_H

#include <QMainWindow>
#include <QtCharts>
#include <QLineSeries>
#include <QChart>
#include <QChartView>
#include <QMouseEvent>
#include "diskiomaster.h"
#include "nettop.h"

namespace Ui {
class DiskStatsChart;
}

class DiskStatsChart : public QMainWindow
{
	Q_OBJECT
	
public:
	explicit DiskStatsChart(QWidget *parent = nullptr);
	~DiskStatsChart();
protected:
	void mousePressEvent(QMouseEvent *event) override;
private:
	Ui::DiskStatsChart *ui;
	DiskioMaster *diskio = nullptr;
	NetTop *net = nullptr;
	QChart *chart = nullptr;
	QChartView *chartView = nullptr;
	QLineSeries *seriesRead = nullptr;
	QLineSeries *seriesWrite = nullptr;
	QLineSeries *seriesReadPower = nullptr;
	QLineSeries *seriesTx = nullptr;
	QLineSeries *seriesRx = nullptr;
	void createChart();
	std::string file;
	std::string files;
	int countDiskClick=0;
	int countInterfaceClick=0;
	QTimer *timer = nullptr;
	QTimer *timerNet = nullptr;
	QDateTimeAxis *axisX = nullptr;
	QValueAxis *axisY = nullptr;
	QListWidget *interfacesList = nullptr;
private slots:
	void on_connectButton_clicked();
	void on_disksList_itemClicked(QListWidgetItem *item);
	void showChart();
	void on_interfacesList_itemClicked(QListWidgetItem *items);
	void showChartNet();
};

#endif // DISKSTATSCHART_H
