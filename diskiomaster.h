#ifndef DISKIOMASTER_H
#define DISKIOMASTER_H

#include <grpc++/grpc++.h>
#include "diskio.grpc.pb.h"
#include <google/protobuf/empty.pb.h>
#include <QObject>

using measuresystems::DiskioMasterService;

class DiskioMaster : public QObject
{
	Q_OBJECT
public:
	DiskioMaster(const std::string &endp);
	~DiskioMaster();
	measuresystems::Disks disks;
	measuresystems::DiskStats GetDiskStats(std::string diskname);
private:
	measuresystems::Disks GetDisks();
	void CreateChannel(const std::string &endp);
	std::unique_ptr<DiskioMasterService::Stub> stub = nullptr;
	std::string endpoint;
};

#endif // DISKIOMASTER_H
