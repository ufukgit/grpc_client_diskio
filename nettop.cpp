#include "nettop.h"

NetTop::NetTop(const std::string &ep)
{
	endpoint = ep;
	CreateChannel(endpoint);
	GetInterfaces();
}

void NetTop::CreateChannel(const std::string &ep)
{
	using grpc::Channel;
	std::shared_ptr<Channel> channel;
	channel = grpc::CreateChannel(ep, grpc::InsecureChannelCredentials());
	stub = NetTopService::NewStub(channel);
}

measuresystems::NetworkInterfaces NetTop::GetInterfaces()
{
	google::protobuf::Empty empty;
	grpc::ClientContext context;
	grpc::Status statusInterfaces;
	measuresystems::NetworkInterfaces interfacesCtrl;
	if (!stub)
		return interfacesCtrl;
	std::mutex tempInterfaces;
	std::lock_guard<std::mutex> lockInterfaces(tempInterfaces);
	statusInterfaces = stub->GetInterfaces(&context, empty, &interfaces);
	return interfaces;
}

measuresystems::BandwidthResponse NetTop::GetBandWidth(std::string getinterface)
{
	grpc::Status statusBandWidth;
	measuresystems::BandwidthResponse bandWidthResp;
	measuresystems::BandwidthResponse bandWidthCtrl;
	measuresystems::NetworkInterface interface;
	grpc::ClientContext contextBandWidth;
	*interface.mutable_name() = getinterface;
	statusBandWidth = stub->GetBandWidth(&contextBandWidth, interface, &bandWidthResp);
	if (!stub)
		return bandWidthCtrl;
	return bandWidthResp;
}

NetTop::~NetTop()
{
}
